# dbup

dbup is a Docker image for a MySQL or PostgreSQL server that automatically backs up and restores your database (using either Percona XtraBackup or PGHoard) to the local file system or an S3-compatible storage backend.

### ⚠️ WARNING: README DRIVEN DEVELOPMENT

**This repository is using [Readme Driven Development](https://tom.preston-werner.com/2010/08/23/readme-driven-development.html), which means that everything you can read here are still ideas and plans for the future! The first public release will be some time in 2021.**

---

## Usage

dbup is based on the official [`postgres`](https://hub.docker.com/_/postgres/) and [`mysql`](https://hub.docker.com/_/mysql/) images (and gets updated automatically as soon as a new relase is coming out), so the basic usage is the same.

If the container is started with an empty database volume, it automatically tries to restore the latest backup. If the container is stopped normally, it tries to create a backup just before shutting down (you might want to set `--stop-timeout` when starting your container if 10 seconds aren't enough for a full backup).

All you need to do to make use of the backup functionality is to add a volume `-v "...:/backup"`.

**MySQL:**
```bash
docker run -d \
  -e MYSQL_ROOT_PASSWORD=my-secret-pw \
  --stop-timeout=500 \
  -v "/mnt/backup/my-database:/backup" \
  codeberg.org/momar/dbup:mysql-8
```

**PostgreSQL:**
```bash
docker run -d \
  -e POSTGRES_PASSWORD=mysecretpassword \
  --stop-timeout=500 \
  -v "/mnt/backup/my-database:/backup" \
  codeberg.org/momar/dbup:postgres-13
```

### Restoring backups
TODO

## Labels
TODO

## Volumes
TODO

## Environment variables

- Backup configuration
  - `BACKUP_INTERVAL`: interval definition, e. g. `3d 12h 40m`, or a set of an optional list of weekdays (every day by default) and a time, separated by comma (`mo-we+sa 04:00`). The default is `3h`.
  - `BACKUP_KEEP`: number of latest backups to keep (you can also explicitly specify e. g. `4latest`), or space-separated list of retention rules (default: `12hourly 7daily 4weekly 6monthly`). If you want to keep e. g. yearly backups forever, add `*yearly`.
- Backup storage
  - `S3_...`
- Backup notifications
  - `BACKUP_NOTIFICATION_EMAIL`
  - TODO: SMTP server
- MySQL
- PostgreSQL